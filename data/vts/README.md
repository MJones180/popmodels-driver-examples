# VT Information

Files in this directory were taken from [https://gitlab.com/dwysocki/pop-models-o3a-aps-april-2021/-/tree/master/data/vts](https://gitlab.com/dwysocki/pop-models-o3a-aps-april-2021/-/tree/master/data/vts).

The file `vt_aLIGO140MpcT1800545_BBH+BNS+NSBH_fine_m1_m2_a1z_a2z.hdf5` was renamed to `vt.hdf5`.
