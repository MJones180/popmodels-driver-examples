# GWTC-2 HDF5 Files

This is where the HDF5 files for GWTC-2 open data are stored.

In addition, this is where the `gwtc_explorer` generated image is outputted.
