# Inference Examples

This directory will contain examples on how to run population inference using the driver.
It is assumed that the [Installation](../#installation) and [Fetch Data](../#fetch-data) sections of the [`README.md`](../README.md) at the root of this repository have already been completed.

The examples all have a number prepended to their name.
This signifies the order in which examples should be followed due to an incremental increase in complexity.
Information on each example can be found within the associated `README.md`.

The first three examples will contain, at the very minimum, an `inference.ini` and `run_inference.sh`.
When you are setting up your own runs, the `inference.ini` can be named anything you would like.
Additionally, the command inside of `run_inference.sh` can be executed directly in the terminal – it does not have to be first added to a shell script.

The config files will all be very minimal, so it can be helpful to look at the config templates (detailed information on the expected values for each config).
They can be found in the `PopModels` repository under `src/pop_models/applications/driver/config_templates/`.

To run any example, simply execute the following command while inside of its directory:

	(venv) $ bash run_inference.sh

This will run the population inference for that example with both verbose logging and debug mode turned on.
