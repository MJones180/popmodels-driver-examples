# ====================================
# Custom defined functions and classes
# ====================================


def my_population(xpy, **otherArgs):
    # Should return a population object
    pass


def my_detection_likelihood(population, detection):
    # Should create a new detection object
    pass


class MyDetectionLoader:
    def load(
        self,
        event_path,
        coord_system,
        weights_field,
        inv_weights_field,
    ):
        # Should create a new loader object
        pass


class MyPoisson():
    def __init__(self, population, VT):
        # Should create a new Poisson object
        pass


class MyPrior():
    def __init__(
        self,
        random_state,
        population,
        prior_settings,
        constants,
        duplicates,
    ):
        # Should initialize all needed instance variables
        pass

    def log_prior(self, params):
        # Should return the accumulated log(prior)
        pass

    def init_walkers(self, walkers_total, pop_inf=None):
        # Should return the initializing variables
        pass

    @staticmethod
    def parse_param_names(
        param_names,
        variables_file,
        constants_file=None,
        duplicates_file=None,
    ):
        # Should return a mapping of parameter names to their dist and params
        pass
