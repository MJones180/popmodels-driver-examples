# Example 4: `custom_functions_and_classes`

All population inference runs allow for customizability.
The following config values allow for a custom function/class:

- `population.function`
- `prior.class`
- `poisson.class`
- `detection.loader`
- `detection.likelihood`

These features are still under beta testing, so bugs may occur.

This example does not run.
Instead, it contains the setup necessary for handling custom functions and classes.
