#!/bin/bash

# Clean the MCMC posterior samples;
# Skip the first 1000 samples, then take every 50 samples after that;
# Overwrite existing `post_cleaned.hdf5` if there already is one
pop_models_extract_samples output/ post_cleaned.hdf5 --fixed-burnin 1000 --fixed-thinning 50 --force
