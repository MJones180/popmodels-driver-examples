#!/bin/bash

# Run population inference using the `inference.ini`;
# Enable debugging and verbose outputs
driver inference inference.ini -dv
