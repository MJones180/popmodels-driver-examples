# Example 1: `test_run`

This is the most basic population inference possible and should only be used to test initial setup.
It is **not recommended** to use this for your own runs.

The config files have all been merged together into one monolithic `inference.ini`.
Generally, config files should be split apart for both reusability and readability.
A value can be omitted from the config by either deleting or commmeting out the line when the default value should be used.
For this run, all default values will be left in the config for completeness, but going forward, default values may be omitted.

This run has been set to use the `MultiSpin` population; only one gaussian and one powerlaw will be used.
For efficiency, a singular event will be used and only 32 samples will be generated.
The posterior samples will be initialized from the prior; this is signified by `posterior.init_type` being set to `None.`
The second example, `multi_spin`, will cover this more in depth, along with how to clean the walked samples.

To run the population inference, call the `run_inference.sh` script.
The population inference will create and populate the `output/` folder with the posterior samples generated from each walker in the ensemble MCMC.
