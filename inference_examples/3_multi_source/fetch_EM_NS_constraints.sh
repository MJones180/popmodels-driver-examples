#!/bin/bash

# ==============================================================================
# File taken from link below.
# https://gitlab.com/dwysocki/pop-models-o3a-aps-april-2021/-/blob/master/runs/MultiSource_EM-informed/fetch_EM_NS_constraints.sh
# ==============================================================================

# Downloads EM constraints on neutron stars from Farr and Chatziioannou (2020)
# doi:10.3847/2515-5172/ab9088
# Renames file to be more descriptive
wget -O NS_EM_constraints.h5 https://github.com/farr/AlsingNSMassReplication/raw/16b3b3eb96c46650e3e48c5c1d130ab6c1b5805d/ar.nc
