# Example 3: `multi_source`

The config files follow the same format as in the second example (`multi_spin`).

This run has been set to use the `MultiSpin` population; three gaussians and one powerlaw will be used.
A total of 256 walkers will be used to crawl the ensemble.
In addition to the `uniform` prior distribution, this run also makes use of the `KDE` prior distribution.

Prior to running population inference, call the `fetch_EM_NS_constraints.sh` script.
This script depends on the `wget` command; it can be installed very easily via `homebrew`, `conda`, or `pip`.
Upon completion, an `.hdf5` file will be downloaded that specifies the constraints on the `m1_max_g2` KDE parameter.

With the constraints file downloaded, population inference can be started by calling the `run_inference.sh` script.
This run uses a `post_cleaned.hdf5` file – an existing, cleaned posterior samples file – to base the MCMC on.
The following values in the `inference.ini` allow for this:

- `posterior.init_data` is set to `post_cleaned.hdf5`
- `posterior.init_type` is to to `init_from_cleaned_samples`

Population inference will create the `output/` folder and 5000 posterior samples will be generated.
