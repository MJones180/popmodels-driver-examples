# Virtual Environment

This folder should be populated with a Python [virtual environment](https://docs.python.org/3/library/venv.html).

Instructions for doing so can be found under the [Installation section of the `README.md`](../#installation) at the root of this repository.
