# Fetch and Preprocess Data

This folder handles the fetching of data via the `gwtc_explorer` package and the conversion to `.dat` files.

Instructions for doing so can be found under the [Fetch Data section of the `README.md`](../#fetch-data) at the root of this repository.
