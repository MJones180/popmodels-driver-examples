#!/bin/bash

# ==============================================================================
# File taken from link below with slight alterations made.
# https://gitlab.com/dwysocki/pop-models-o3a-aps-april-2021/-/blob/master/bin/fetch_and_plot_gwtc2.sh
# ==============================================================================

# Uses GWTC-explorer to download GWTC-2 and make a simple plot.
gwtc_explorer scatter_2d \
    --save-fig ../data/hdf5/GWTC2_scatter_m1m2.png \
    --catalog-min 1 --catalog-max 2 \
    --event-dir ../data/hdf5/ \
    --parameters mass_1_source mass_2_source
