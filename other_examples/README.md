# Other Examples

This folder contains examples on using the `event_contribution`, `observable_quantiles`, and `ppd` driver scripts.
The config files for each example contain basic, yet opinionated run options.

Each example uses the population from [`/inference_examples/2_multi_spin`](/inference_examples/2_multi_spin), along with the `post_cleaned.hdf5` that it generates.
These files are located in the [`shared_files/`](shared_files/) directory.

Important note: each of the driver scripts detailed in these examples pull information on the model from the `post_cleaned.hdf5`, not the population itself.
