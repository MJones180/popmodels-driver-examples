# popmodels-driver-examples

This repository will provide instructions on how to install `PopModels`, download the GWTC-2 data, and run scripts using the driver.
The driver provides population inference and other useful scripts using a mix of the CLI and configuration files.
More information on the driver can be found in the [Driver User Guide](https://bayesian-parametric-population-models.readthedocs.io/en/latest/driver/index.html) within the [`PopModels` documentation](http://bayesian-parametric-population-models.readthedocs.io/en/latest/).

Please refer to the config templates for information on the expected contents for each driver configuration file.
They can be found in the `bayesian-parametric-population-models` (`PopModels`) repository under `src/pop_models/applications/driver/config_templates/`.

**Important Note:**\
Some files and text from this repository were copied over from the [`pop-models-o3a-aps-april-2021`](https://gitlab.com/dwysocki/pop-models-o3a-aps-april-2021/-/tree/master/) repository written by Daniel Wysocki.

## Installation

A Python [virtual environment](https://docs.python.org/3/library/venv.html) should be created to store all dependencies.

You will need Python 3.6 or above to run everything successfully.  We'll assume your python executable is called `python`, but it might be something else (e.g., `python3`, `python3.8`), so replace all of the `python` commands with whatever yours is called.  You will also want to have `pip` installed for your Python environment.  If you don't know if you have `pip`, you can either install it according to your operating system's instructions, or simply run:

	$ python -m ensurepip

Now, create a virtual environment that will be located in the `venv/` folder:

	$ python -m venv venv

Whenever you want to run this demo, make sure the virtual environment is activated for your session:

	$ source venv/bin/activate

You can deactivate it at any time using the `deactivate` command, but don't do that yet.

Now, we will enter the virtual environment so that we can download and install the `PopModels` and `GWTC-Explorer` repositories:

	(venv) $ cd venv

First, we will take care of the `PopModels` package, this will handle the population inference:

	(venv) $ git clone https://gitlab.com/dwysocki/bayesian-parametric-population-models.git PopModels
	(venv) $ cd PopModels
	(venv) $ python -m pip install .
	(venv) $ cd ..

Next, we will handle the `GWTC-Explorer` package, this will fetch all of the data:

	(venv) $ git clone https://git.ligo.org/daniel.wysocki/gwtc-explorer.git
	(venv) $ cd gwtc-explorer
	(venv) $ python -m pip install .
	(venv) $ cd ..

Once both packages are taken care of, we can go back to the root of the repository:

    (venv) $ cd ..

Congratulations, initial setup is now complete!

## Fetch Data

The next step is to fetch all of the GWTC-2 data and convert it to `.dat` text files.
Luckily, there are some handy scripts which will help accomplish this in the `fetch_and_preprocess_data/` directory.

The first step will be to enter the correct directory:

	(venv) $ cd fetch_and_preprocess_data

Now, we can run the shell script that will fetch all of the data using the `GWTC-Explorer` package (this will take a while to run):

	(venv) $ bash fetch_and_plot_gwtc2.sh

Upon completion, the [`data/hdf5/`](data/hdf5/) directory will be populated with the GWTC-2 `.hdf5` data files.

Since `PopModels` population inference expects all events to be in `.dat` files, we must convert the downloaded `.hdf5` files.
This can be accomplished by running the following `python` script:

	(venv) $ python convert_hdf5_to_dat.py

This will result in the [`data/dat/`](data/dat/) directory being populated with the newly generated `.dat` files.


Once all data is downloaded and converted, we can go back to the root of the repository:

    (venv) $ cd ..

Congratulations, you are ready to begin population inference!

## Examples

The [`inference_examples/`](inference_examples/) folder contains examples on how to run population inference.

The [`other_examples/`](other_examples/) folder contains examples on how to run the other driver scripts.
